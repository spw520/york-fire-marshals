package com.firemarshalls.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;

public class Player {

    //Vector2 holds an x and a y value, used for sprite coordinates
    //the movement velocity of the sprite
    private Vector2 velocity = new Vector2();

    private float speed = 300f, increment;
    private String blockedKey = "blocked";
    public FireTruck activeTruck;
    public Sprite sprite;

    private TiledMapTileLayer collisionLayer;

    //constructor for the player
    public Player(FireTruck truck, Sprite sprite, TiledMapTileLayer collisionLayer) {
        this.sprite = sprite;
        this.sprite.setSize(32 * 3,32 * 3);
        this.activeTruck=truck;
        this.collisionLayer = collisionLayer;
        this.sprite.setSize(collisionLayer.getWidth() * 2, collisionLayer.getHeight() * 3);
    }

    //calculate the next position of the sprite based on velocity and current pos
    public void update(float delta) {
        //clamp velocity to a max value
        if (velocity.y > speed)
            velocity.y = speed;
        else if (velocity.y < -speed)
            velocity.y = -speed;

        if(Gdx.input.isKeyPressed(Input.Keys.W)) {
            velocity.y = speed;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.A)) {
            velocity.x = -speed;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.S)) {
            velocity.y = -speed;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.D)){
            velocity.x = speed;
        }

        //save old position
        float oldX = sprite.getX(), oldY = sprite.getY();
        boolean collisionX = false, collisionY = false;

        // move on x
        sprite.setX(sprite.getX() + velocity.x * delta);

        // calculate the increment for step in #collidesLeft() and #collidesRight()
        increment = collisionLayer.getTileWidth();
        increment = sprite.getWidth() < increment ? sprite.getWidth() / 2 : increment / 2;

        if(velocity.x < 0) // going left
            collisionX = collidesLeft();
        else if(velocity.x > 0) // going right
            collisionX = collidesRight();

        // react to x collision
        if(collisionX) {
            sprite.setX(oldX);
            velocity.x = 0;
        }

        // move on y
        sprite.setY(sprite.getY() + velocity.y * delta);

        // calculate the increment for step in #collidesBottom() and #collidesTop()
        increment = collisionLayer.getTileHeight();
        increment = sprite.getHeight() < increment ? sprite.getHeight() / 2 : increment / 2;

        if(velocity.y < 0) // going down
            collisionY = collidesBottom();
        else if(velocity.y > 0) // going up
            collisionY = collidesTop();

        // react to y collision
        if(collisionY) {
            sprite.setY(oldY);
            velocity.y = 0;
        }

        velocity.x = 0;
        velocity.y = 0;
    }


    private boolean isCellBlocked(float x, float y) {
        System.out.println(x / collisionLayer.getTileWidth() / 3);
        TiledMapTileLayer.Cell cell = collisionLayer.getCell((int) (x / collisionLayer.getTileWidth()) / 3, (int) (y / collisionLayer.getTileHeight() / 3));
        return cell != null && cell.getTile() != null && cell.getTile().getProperties().containsKey(blockedKey);
    }

    public boolean collidesRight() {
        for(float step = 0; step <= sprite.getHeight(); step += increment)
            if(isCellBlocked(sprite.getX() + sprite.getWidth(), sprite.getY() + step))
                return true;
        return false;
    }

    public boolean collidesLeft() {
        for(float step = 0; step <= sprite.getHeight(); step += increment)
            if(isCellBlocked(sprite.getX(), sprite.getY() + step))
                return true;
        return false;
    }

    public boolean collidesTop() {
        for(float step = 0; step <= sprite.getWidth(); step += increment)
            if(isCellBlocked(sprite.getX() + step, sprite.getY() + sprite.getHeight()))
                return true;
        return false;

    }

    public boolean collidesBottom() {
        for(float step = 0; step <= sprite.getWidth(); step += increment)
            if(isCellBlocked(sprite.getX() + step, sprite.getY()))
                return true;
        return false;
    }


    public Vector2 getVelocity(){
        return velocity;
    }

    public void setVelocity(Vector2 velocity){
        this.velocity = velocity;
    }

    public float getSpeed(){
        return speed;
    }

    public void setSpeed(float speed){
        this.speed = speed;
    }

    public TiledMapTileLayer getCollisionLayer(){
        return collisionLayer;
    }

    public void setCollisionLayer(TiledMapTileLayer collisionLayer){
        this.collisionLayer = collisionLayer;
    }
}
