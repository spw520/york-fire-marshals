package com.firemarshalls;

import com.badlogic.gdx.Game;
import com.firemarshalls.screens.BaseBattle;
import com.firemarshalls.screens.MapScreen;

public class RunClass extends Game {
    //definitions
    int num;
    boolean paused;
    private Game game;

    @Override
    public void create () {
        this.game = this;
        num = 10;
        paused = false;
    }

    public void finishGame(boolean vic) {
        if (vic) System.out.print("game over, you win!!");
    }

    @Override
    public void render () {
        if(!paused){
            System.out.println(num);
            num--;
            if (num==0) {
                paused=true;
                setScreen(new MapScreen(game));
            }
        }
        if(paused){
            super.render();
        }
    }

    public void pause() {
        super.pause();
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
